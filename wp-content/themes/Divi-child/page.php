<?php

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

	<div id="main-content">

		<div class="container">




			<?php if ( ! $is_page_builder_used ) : ?>
			<div id="content-area" class="clearfix">
				<div id="left-area-page">



					<?php endif; ?>


					<div id="left-area">
						<div class="my_breadcrumb breadcrumb-<?php the_ID(); ?>">

							<?php
							ob_start();
							$parent_title = get_the_title($post->post_parent);
							if($parent_title=="Archives"):
								//do something
							else:
								//do something else
							endif;

							if ( !is_front_page() ) {
								echo '<ul class="breadcrumb"><span class="breadcrumb_info"></span><a href="';
								echo get_option('home');
								echo '">';
								bloginfo('name');
								echo "</a>";
								echo "<i class=\"fa fa-chevron-right\"></i>";
								echo "<span>";
								echo $parent_title;
								echo "</span>";
								echo "<i class=\"fa fa-chevron-right\"></i>";
							}
							echo "<span class='active'>";
							if ( is_category() || is_single() ) {
								$category = get_the_category();
								$ID = $category[0]->cat_ID;
								echo get_category_parents($ID, TRUE, ' &raquo; ', FALSE );
							}

							if(is_single() || is_page()) {the_title();}
							if(is_tag()){ echo "Tag: ".single_tag_title('',FALSE); }
							if(is_404()){ echo "404 - Page not Found"; }
							if(is_search()){ echo "Search"; }
							if(is_year()){ echo get_the_time('Y'); }
							echo "</span>";

							echo "</ul>";
							?>
						</div>
						<?php while ( have_posts() ) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

								<?php if ( ! $is_page_builder_used ) : ?>

									<h1 class="entry-title main_title"><?php the_title(); ?></h1>
									<?php
									$thumb = '';

									$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

									$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
									$classtext = 'et_featured_image';
									$titletext = get_the_title();
									$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
									$thumb = $thumbnail["thumb"];

									if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
										print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
									?>

								<?php endif; ?>

								<div class="entry-content">
									<?php
									the_content();

									if ( ! $is_page_builder_used )
										wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
									?>
								</div> <!-- .entry-content -->

								<?php
								if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
								?>

							</article> <!-- .et_pb_post -->

						<?php endwhile; ?>
					</div>
					<?php get_sidebar(); ?>

				</div> <!-- #left-area -->


			</div> <!-- #content-area -->
		</div> <!-- .container -->



	</div> <!-- #main-content -->

<?php

get_footer();
