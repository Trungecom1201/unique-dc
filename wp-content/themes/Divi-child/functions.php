<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}


add_action( 'et_header_top', 'et_add_mobile_navigation' );

function ws_scripts_and_styles() {
    wp_enqueue_script('jquery-js', get_stylesheet_directory_uri() .'/assets/js/jquery.min.js', array(), time() );
    wp_enqueue_script('custom-min-js', get_stylesheet_directory_uri() .'/js/custom.min.js', array(), time() );
    wp_enqueue_style('bootstrap-style', get_stylesheet_directory_uri() .'/assets/css/bootstrap.min.css');
    wp_enqueue_style('custom-style', get_stylesheet_directory_uri() .'/assets/css/custom.css');
    wp_enqueue_style('styles-style', get_stylesheet_directory_uri() .'/assets/css/styles.css');
    wp_enqueue_style( 'fontawsome-style', get_stylesheet_directory_uri() . "/assets/font-awesome-4.7.0/css/font-awesome.min.css", array(),time() );
    wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() .'/assets/js/bootstrap.min.js', array(), time());
    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() .'/assets/js/theme.js', array(), time(), true, true);

}
add_action( 'wp_enqueue_scripts', 'ws_scripts_and_styles' );

function ws_scripts_and_styles_footer() {
    wp_enqueue_style('slick-css', get_stylesheet_directory_uri() .'/assets/css/slick.css');
    wp_enqueue_script('slick-min-js', get_stylesheet_directory_uri() .'/assets/js/slick.min.js', array(), time());
    wp_enqueue_script('custom-js-script', get_stylesheet_directory_uri() .'/assets/js/custom.js', array(), time());
}
add_action( 'wp_footer', 'ws_scripts_and_styles_footer' );


add_shortcode( 'short_code_blog', 'short_code_blog' );

function short_code_blog(){
    ob_start();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'category_name' => 'Blog', 'posts_per_page' => 3, 'paged' => $paged);
    $loop = new WP_Query( $args );
    if( $loop->have_posts()){ ?>
        <?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="item-slider-post">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </div>
            <?php
        endwhile; ?>
    <?php }
    wp_reset_query();
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}


add_shortcode( 'short_code_quicklinks', 'short_code_quicklinks' );

function short_code_quicklinks(){
    ob_start();
    $args = array( 'category_name' => 'Quicklinks' );
    $loop = new WP_Query( $args );
    if( $loop->have_posts()){ ?>
        <div class="quicklinks-post"><h2 class="heading-quicklinks">Quicklinks</h2>
            <?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="item-slider-post <?php the_title(); ?>">
                    <div class="sub-post-slider-homepage">
                        <div class="link-header">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="content-post">
                            <div class="img-content-post">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                            </div>
                            <div class="description-post">
                                <?php echo the_content();?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endwhile; ?>
        </div>
    <?php }
    wp_reset_query();
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}

add_shortcode( 'short_code_team', 'short_code_team' );

function short_code_team(){
    ob_start();
    $args = array( 'category_name' => 'Team', 'posts_per_page' => 1, 'paged' => $paged);
    $loop = new WP_Query( $args );
    if( $loop->have_posts()){ ?>
        <div class="block-team-post">
            <?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="item-team-post">
                    <div class="link-header">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </div>
                    <div class="content-post">
                        <div class="img-content-post">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                        </div>
                        <div class="description-post">
                            <?php echo the_content();?>
                        </div>
                        <a class="link-team-post" href="<?php the_permalink(); ?>">Aktuelles Stellenangebot</a>
                    </div>
                </div>
                <?php
            endwhile; ?>
        </div>
    <?php }
    wp_reset_query();
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}


function languages_list_footer(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        echo '<div id="footer_language_list"><ul>';
        foreach($languages as $l){
            echo '<li>';
            if($l['country_flag_url']){
                if(!$l['active']) echo '<a href="'.$l['url'].'">';
                echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
                if(!$l['active']) echo '</a>';
            }
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo icl_disp_language($l['native_name'], $l['translated_name']);
            if(!$l['active']) echo '</a>';
            echo '</li>';
        }
        echo '</ul></div>';
    }
}

?>