$('#main-header .et_menu_container #top-menu>li').click(function(){
    $(this).toggleClass('sub-item-active');
//     $(this).children().next().toggleClass('active-menu');
});

setTimeout(function(){
    $( ".country-selector.weglot-default" ).insertAfter( "#main-header .et_menu_container #et-top-navigation" );
}, 200);


$("#et-top-navigation .quick-link-mobile").click(function(){
    $('#main-header .et_menu_container #top-menu-nav').removeClass('open');
    $("#main-content #left-area").toggleClass('left-area-active');
    $('#main-content #sidebar').toggleClass('sidebar-active');
});


$('#et-top-navigation .mobile_menu_bar').click(function(){
    $('#main-header .et_menu_container #top-menu-nav').toggleClass('open');
});

var quicklinks = $('.quicklinks-post .item-slider-post');

for( var i=0; i< quicklinks.length; i++ ){
    var sub = $(quicklinks[i]);
    var find = sub.find('.description-post p').length;

    if(find <= 0){
        sub.addClass('img-content-post-active');
    }
}


var height = $('#main-header .et_menu_container').offset().top;

$(window).scroll(function(){

    var scroll = $(window).scrollTop();

    if(scroll > height ){
        $('#main-header .header-menu-clearfix').addClass('active-header');
    }
    else{
        $('#main-header .header-menu-clearfix').removeClass('active-header');
    }
});


$('.mobile_menu_bar.mobile_menu_bar_toggle').text('Menu');

$("article .post-content .more-link").text('Mehr Lesen');

$('#sidebar #icl_lang_sel_widget-2').appendTo($('.header-menu-clearfix .wpml-language'));